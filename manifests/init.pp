 class tika (
  $version      = $tika::params::version,
  $install_path = $tika::params::install_path,
  $user         = $tika::params::user,
  $group        = $tika::params::group,
) inherits tika::params {

  file { $install_path:
    ensure  => 'directory',
    mode    => "0775",
    owner   => $user,
    group   => $group,
  }

  include wget

  wget::fetch { "download-tika-$version":
    source      => "http://archive.apache.org/dist/tika/tika-app-$version.jar",
    destination => "$install_path/tika-app-$version.jar",
    timeout     => 0,
    verbose     => false,
    require     => File[$install_path],
  }

 file { 'tika-app-sym-link':
    path    => "$install_path/tika-app.jar",
    ensure  => link,
    target  => "$install_path/tika-app-$version.jar",
    require => Wget::Fetch["download-tika-$version"],
  }

}
