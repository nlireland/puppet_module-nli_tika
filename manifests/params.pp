class tika::params {
  $version      = '1.10'
  $install_path = '/usr/local/tika'
  $user         = 'tika'
  $group        = 'tika'
}
