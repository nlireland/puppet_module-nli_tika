# README #

A puppet module to install the Apached Tika runnable jar file.

The module is create the $install_path folder, download the specified version of the Apache Tika jar and crate a sym link at $install_path/tika-app.jar

## Usage ##

```
#!puppet

node 'mynode' {
  class { 'tika':
    version       => '1.8',       # default is 1.10
    install_path  => '/opt/tika', # default is /usr/local/tika
    user          => 'myuser',    # default is tika
    group         => 'mygroup',   # default is tika
  }
}
```

Or, if your params are set in Hiera:


```
#!puppet

node 'mynode' {
  include tika
}
```

